default:
  image: registry.gitlab.com/gemseo/dev/gemseo/multi-python

# Stages executed sequentially until a job fails.
stages:
  - check
  - tests

variables:
  TEST_ALL_ENVS:
    value: "false"
    description: "Whether to run the tox tests for all python versions and all platforms."
  MATLAB_PYTHON_WRAPPER_WINDOWS:
      value: ""
      description: Enable with git+file:////C:\python-matlab@windows ; python_version<'3.9'

workflow:
  # Prevent duplicated pipelines,
  # see https://docs.gitlab.com/14.3/ee/ci/yaml/index.html#switch-between-branch-pipelines-and-merge-request-pipelines.
  rules:
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"'
      when: never
    - when: always

# Jobs are defined as a hierarchy from the most common settings to the specific ones.
# TODO: make the hierarchy more readable
# .base
# .linux
# .windows
# .extended-tests
#     check
#     pylint
#     .tests
#         .tests-linux
#             py39
#             py37,py38
#         .tests-windows
#             py39-win
#             py37-win,py38-win

# Base job template.
.base:
  interruptible: true
  script:
    # Run a tox env named after a job name.
    - tox -e $TOX_ENV_NAME

# Specific settings for linux jobs.
.linux:
  tags:
    - gemseo-docker
    - docker
  variables:
    # Caches on the CI runner host.
    PIP_CACHE_DIR: "/opt/gitlab-runner-cache/pip"

# Specific settings for windows jobs.
.windows:
  tags:
    - gemseo-windows-server-2012
    - windows-server-2012
  variables:
    # Caches on the CI runner host.
    PIP_CACHE_DIR: "C:\\pip"
    MATLAB_PYTHON_WRAPPER: $MATLAB_PYTHON_WRAPPER_WINDOWS

# Jobs for the check stage.
check:
  extends:
    - .base
    - .linux
  stage: check
  variables:
    PRE_COMMIT_HOME: "$CI_PROJECT_DIR/.cache/pre-commit"
    # Pass the cache locations through the tox env.
    TOX_TESTENV_PASSENV: PIP_CACHE_DIR PRE_COMMIT_HOME
    TOX_ENV_NAME: check

# Tests stage.

# Base tests job, common to all the tests jobs.
.tests:
  extends: .base
  stage: tests
  variables:
    COVERAGE_FILE: coverage.xml
    # The junitxml is for showing the number of tests if gitlab UI.
    PYTEST_ADDOPTS: --junitxml=report.xml
    TOX_TESTENV_PASSENV: PYTEST_ADDOPTS
  artifacts:
    # Send the artifacts even on job failure.
    when: always
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: $COVERAGE_FILE
  # To get the total coverage shown in gitlab UI.
  coverage: '/^TOTAL.+?(\d+\%)$/'
  rules:
    # Only run a test job when the following files have changed.
    - changes:
      - src/**/*.*
      - tests/**/*.*
      - requirements/test.txt
      - setup.cfg
      - tox.ini
      - .gitlab-ci.yml
  after_script:
    - codecov -t $CODECOV_TOKEN -f $COVERAGE_FILE --required

# Common settings for windows tests jobs.
.tests-windows:
  extends:
    - .tests
    - .windows

# Common settings for linux tests jobs.
.tests-linux:
  extends:
    - .tests
    - .linux

# Jobs for the tests stage.

.base-tests:
  variables:
    TOX_ENV_NAME: py39-coverage

base-linux:
  extends:
    - .tests-linux
    - .base-tests

base-windows:
  extends:
    - .tests-windows
    - .base-tests

# Define jobs for minor python env with this extended parent.
.extended-tests:
  rules:
    # For selecting from gitlab UI.
    - if: '$TEST_ALL_ENVS == "false"'
      when: never
    # Always run for commits pushed on the master branch.
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
    - !reference [.tests, rules]
  parallel:
    matrix:
      - TOX_ENV_NAME:
        - py37-coverage
        - py38-coverage

extended-linux:
  extends:
    - .tests-linux
    # Last so its rules applies.
    - .extended-tests

extended-windows:
  extends:
    - .tests-windows
    # Last so its rules applies.
    - .extended-tests
