..
   Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com

   This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit
   http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
   Commons, PO Box 1866, Mountain View, CA 94042, USA.

.. uml::

	abstract class MLClassificationAlgo {
	 +n_classes
	 +learn()
	 +predict_proba()
	 #predict_proba()
	 #predict_proba_hard()
	 #predict_proba_soft()
	 #get_objects_to_save()
	}

	class KKNClassifier {
	 +LIBRARY
	 +ABBR
	 +algo
	 +n_neighbors
	 #fit()
	 #predict()
	 #predict_proba_soft()
	}

	class RandomForestClassifier {
	 +LIBRARY
	 +ABBR
	 +algo
	 +n_estimators
	 #fit()
	 #predict()
	 #predict_proba_soft()
	}

	class SVMClassifier {
	 +LIBRARY
	 +ABBR
	 +algo
	 +C
	 +kernel
	 +probability
	 #fit()
	 #predict()
	}

	MLClassificationAlgo <|-- KKNClassifier
	MLClassificationAlgo <|-- RandomForestClassifier
	MLClassificationAlgo <|-- SVMClassifier
